import { Fragment, StrictMode } from 'react';
import { useSelector } from 'react-redux';
import Counter from './components/Counter';
import Header from './components/Header';
import Auth from './components/Auth';
import UserProfile from './components/UserProfile';
import SignIn from './components/SignIn';
import Chart from './components/Chart';

function App() {
  const isAuth = useSelector(state => state.auth.isAuthenticated);
  const isSigIn = useSelector(state => state.auth.isRegistered);
  return (
    <Fragment>
      <Header />
      {!isSigIn && !isAuth && <SignIn />}
      {!isAuth && isSigIn && <Auth />}
      {/* {isAuth && <UserProfile />} */}
      {/* {isAuth && <Counter />} */}
      <div style={{ display: 'flex', justifyContent: 'center', paddingTop: '3%' }}>
        <Chart />
      </div>
    </Fragment>
  );
}

export default App;
