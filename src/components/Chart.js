import React from 'react';
import { LineChart, Line, XAxis, YAxis, CartesianGrid, Tooltip, Legend, ResponsiveContainer } from 'recharts';

const data = [
  {
    name: 'Time A',
    uv: 4000,
    time: 2400,
    amt: 2400,
  },
  {
    name: 'Time B',
    uv: 3000,
    time: 1398,
    amt: 2210,
  },
  {
    name: 'Time C',
    uv: 2000,
    time: 9800,
    amt: 2290,
  },
  {
    name: 'Time D',
    uv: 2780,
    time: 3908,
    amt: 2000,
  },
  {
    name: 'Time E',
    uv: 1890,
    time: 4800,
    amt: 2181,
  },
  {
    name: 'Time F',
    uv: 2390,
    time: 3800,
    amt: 2500,
  },
  {
    name: 'Time G',
    uv: 3490,
    time: 4300,
    amt: 2100,
  },
  {
    name: 'Time 8',
    uv: 3490,
    time: 4300,
    amt: 2100,
  },
  {
    name: 'Time 9',
    uv: 3490,
    time: 4300,
    amt: 2100,
  },
  {
    name: 'Time 10',
    uv: 3490,
    time: 4300,
    amt: 2100,
  },
];

export default function Chart() {
  return (
    <LineChart
      width={500}
      height={300}
      data={data}
      margin={{
        top: 5,
        right: 30,
        left: 20,
        bottom: 5,
      }}
    >
      <CartesianGrid strokeDasharray="3 3" />
      <XAxis dataKey="name" />
      <YAxis />
      <Tooltip />
      <Legend />
      <Line type="monotone" dataKey="time" stroke="#f4f0fa" activeDot={{ r: 10 }} />
    </LineChart>
  );
}
