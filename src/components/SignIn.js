import { useDispatch } from 'react-redux';
import classes from './SignIn.module.css';
import { authActions } from '../store/auth';

const SignIn = () => {
  const dispatch = useDispatch();
  const loginHandler = event => {
    event.preventDefault();
    dispatch(authActions.login());
  };
  const isRegistered = () => {
    dispatch(authActions.isRegistered());
  };

  return (
    <main className={classes.auth}>
      <section>
        {/*on register in future*/}
        <form onSubmit={loginHandler}>
          <div className={classes.control}>
            <label htmlFor="username">username</label>
            <input type="username" id="username" />
          </div>
          <div className={classes.control}>
            <label htmlFor="email">Email</label>
            <input type="email" id="email" />
          </div>
          <div className={classes.control}>
            <label htmlFor="password">Password</label>
            <input type="password" id="password" />
          </div>
          <div className={classes.control}>
            <label htmlFor="password">Confirm Password</label>
            <input type="password" id="password" />
          </div>
          <div className={classes.controler}>
            <button>Register</button>
            <h4 onClick={isRegistered}>Login</h4>
          </div>
        </form>
      </section>
    </main>
  );
};

export default SignIn;
