import { useDispatch } from 'react-redux';
import classes from './Auth.module.css';
import { authActions } from '../store/auth';

const Auth = () => {
  const dispatch = useDispatch();

  const loginHandler = event => {
    event.preventDefault();
    dispatch(authActions.login()); //login is valid
  };
  const isNotRegistered = () => {
    dispatch(authActions.isNotRegistered());
  };
  return (
    <main className={classes.auth}>
      <section>
        <form onSubmit={loginHandler}>
          <div className={classes.control}>
            <label htmlFor="email">Email</label>
            <input type="email" id="email" />
          </div>
          <div className={classes.control}>
            <label htmlFor="password">Password</label>
            <input type="password" id="password" />
          </div>
          <div className={classes.controler}>
            <button>Login</button>
            <h4 onClick={isNotRegistered}>Register</h4>
          </div>
        </form>
      </section>
    </main>
  );
};

export default Auth;
