import { createSlice } from '@reduxjs/toolkit';
const initialAuthState = {
  isAuthenticated: false,
  isRegistered: false,
};
const authSlice = createSlice({
  name: 'authentication',
  initialState: initialAuthState,
  reducers: {
    login(state) {
      state.isAuthenticated = true;
    },
    logout(state) {
      state.isAuthenticated = false;
    },
    isRegistered(state) {
      state.isRegistered = true;
    },
    isNotRegistered(state) {
      state.isRegistered = false;
    },
  },
});

export const authActions = authSlice.actions;
export default authSlice.reducer;
